let http = require("http");

http.createServer(function(request,response){

	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Welcome to Booking System");

	}else if(request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Welcome to you profile!");

	}else if(request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Here's our courses available");
	};

// AddCourse POST Method

	if(request.url == "/addCourse" && request.method == "POST"){

		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Add course to our resources");
	};

// updateCourse with PUT method

	if(request.url == "/updateCourse" && request.method == "PUT"){

		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Update a course to our resources");
	};

//

	if(request.url == "/archiveCourse" && request.method == "DELETE"){

		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end("Archive courses to our resources");

	};

}).listen(4000);

console.log("Now connected to localHost Server 4000");